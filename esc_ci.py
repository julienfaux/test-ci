#!/usr/bin/env python3
# version 1.0.0

try:
    import requests
except ImportError:
    from pip import main

    main(["install", "requests"])
    import requests

try:
    from requests_toolbelt.multipart.encoder import MultipartEncoder
except ImportError:
    from pip import main

    main(["install", "requests_toolbelt"])
    from requests_toolbelt.multipart.encoder import MultipartEncoder

import argparse
import json
import os
import time
from inspect import stack

MODES = ["upload", "launch", "harvest", "upload_new"]
DEVICES_KIND = ["ANDROID", "IOS"]
USAGE = f"""
esChecker CI/CD python script


# Usages / Modes:

works with python 3.8+

- ensure user is created by logging in on https://eschecker.eshard.com/
- create an application, set its uuid
- create a campaign, set its uuid
- once an user has been registered, please visit https://eschecker.eshard.com/rest-api,
- generate api key, copy to clipboard and pass it to api_key variable and click lock button Authorize 
- get the file path and fill it

# Examples

## App Uploading

`python3 {__file__} -m upload -a [app_uuid] -k [api_key] -f [app_path]`

## New App uploading

`python3 {__file__} -m upload_new -a [app_uuid] -k [api_key] -f [app_path] -d [device_kind] -n [app name] -i [icon] -d [description]`

## Campaign launching 

`python3 {__file__} -m launch -c [campaign_uuid] -k [api_key] -t [timeout] -a [app_uuid]`

## Campaign results harvesting

`python3 {__file__} -m harvest -k [api_key] -a [app_uuid] -c [campaign_uuid]`

"""

ESCHECKER_URL = "https://dev.eschecker.eshard.com/"


class CheckerCI:
    FINAL_STATES = ['SUCCESS', 'FAILURE', 'ERROR', "CANCELED"]
    DEVICES = ['ANDROID', 'IOS']

    def __init__(self,
                 file_path: str,
                 app_uuid: str,
                 campaign_uuid: str,
                 api_key: str,
                 timeout: int,
                 mode: str,
                 device: str,
                 app_name: str,
                 runid: str,
                 desc: str,
                 icon: str
                 ):
        self.app_uuid = app_uuid
        self.campaign_uuid = campaign_uuid
        self.api_key = api_key
        self.mode = mode
        self.timeout = timeout
        self.file_path = file_path
        self.target = ESCHECKER_URL
        self.device = device
        self.runid = runid
        self.desc = desc
        self.icon = icon
        self.headers = {
            'Authorization': f'x-api-key {api_key}',
            'accept': 'application/json',
            'Content-Type': 'application/json'
        }
        self.post_app_file_headers = {}
        self.step = 10
        # noinspection PyBroadException
        try:
            if app_name:
                self.app_name = app_name
            else:
                self.app_name = self.file_path.split('/')[-1]
        except Exception:
            self.app_name = "app"

    # assertions
    def assertions_on_campaign_uuid(self):
        assert self.campaign_uuid, "campaign uuid missing"
        assert len(self.campaign_uuid) == 36, "wrong campaign_uuid"

    def assertions_on_timeout(self):
        assert self.timeout and type(self.timeout) is int, 'error in timeout parameter'

    def assertions_on_campaign_launch(self):
        self.assertions_on_campaign_uuid()
        self.assertions_on_timeout()

    def assertion_on_file_path(self):
        assert os.path.exists(self.file_path), f"{self.file_path} does not exist"

    def assertion_on_type(self):
        assert self.device, "please fill a device kind"
        assert self.device in self.DEVICES, f"device must be one of {self.DEVICES}"

    def assertions_on_api_key(self):
        assert self.api_key, "api_key missing"
        assert len(self.api_key.split('.')) == 3, "wrong api_key format"

    def assertions_on_app_uuid(self):
        assert self.app_uuid, "app_uuid missing"
        assert len(self.app_uuid) == 36, "wrong app_uuid"

    def assertions_on_upload(self):
        self.assertions_on_app_uuid()
        self.assertion_on_file_path()

    def assertions_on_inputs(self):
        assert self.mode, "please pick a mode"
        assert "eschecker" in self.target and "eshard" in self.target, "error in target"
        self.assertions_on_api_key()
        if self.mode in MODES:
            print(f"working in {self.mode} mode...")
        else:
            print(f'unknown {self.mode} mode')
            exit(-1)

    @staticmethod
    def check_result(rq, name):
        assert 200 <= rq.status_code <= 201, f"{name}: expected 200, got {rq.status_code}"
        assert rq.json(), f"api did not return a json object on {rq.url}"

    # wrapper methods
    def post_app_file(self):
        multipart_data = MultipartEncoder(
            fields={'app': ('app', open(self.file_path, 'rb'), 'application/octet-stream')})
        headers = self.headers
        headers['accept'] = 'application/json'
        headers['Content-Type'] = multipart_data.content_type

        rq = requests.post(f"{self.target}api/apps/{self.app_uuid}/file",
                           headers=headers,
                           data=multipart_data)
        self.check_result(rq, stack()[0][3])
        assert rq.json() == {'status': 'ok'}
        print(f"app (UUID: {self.app_uuid}) updated.")

    def post_campaign_run(self):
        rq = requests.post(url=f"{self.target}api/campaignRuns/",
                           json={"appId": self.app_uuid, "campId": self.campaign_uuid},
                           headers=self.headers)
        self.check_result(rq, stack()[0][3])
        print(f"campaign uuid {rq.json()['id']} launched.")
        return rq

    def write_campaign(self, run_uuid, campaign_run):
        with open(f"{self.app_name}-{run_uuid}.json", "w") as f:
            f.write(json.dumps(campaign_run.json(), ensure_ascii=False, indent=4))

    def output_campaign_results(self, get_campaign_run):
        self.check_result(get_campaign_run, stack()[0][3])
        states = []
        parts = list(get_campaign_run.json()["results"].keys())
        for part in parts:
            print(f"--- {part.upper()} PARTS ---")
            for result in get_campaign_run.json()["results"][part]:
                print(f"{result['testId']} - {result['escRunState']}")
                states.append(result['escRunState'] in self.FINAL_STATES)
        return states

    def get_campaign_run(self, http_get_api_campaign_run_url):
        return requests.get(url=http_get_api_campaign_run_url, headers=self.headers)

    def check_campaign_run_evolution(self, new_run_uuid):
        time.sleep(self.step)
        try:
            print(f"starting with timeout of {self.timeout} sec. and step of {self.step} sec.")
            t = 0
            states = [False]
            http_get_api_campaign_run_url = f"{self.target}api/campaignRuns/{new_run_uuid}/"
            get_campaign_run = ""
            while t < self.timeout and not all(states):
                get_campaign_run = self.get_campaign_run(http_get_api_campaign_run_url)
                states = self.output_campaign_results(get_campaign_run)
                if not all(states):
                    time.sleep(self.step)
                    t += self.step
            try:
                self.write_campaign(run_uuid=new_run_uuid, campaign_run=get_campaign_run)
            except Exception as e:
                raise TimeoutError(f"{e}")
        except Exception as e:
            print(f"An error occured: {e}")

    def launch_campaign(self):
        try:
            campaign_run = self.post_campaign_run()
            new_run_uuid = campaign_run.json()['id']
            self.check_campaign_run_evolution(new_run_uuid)
        except AssertionError as e:
            print(f'problem occured during campaign launch : {e}')

    def upload_app(self):
        try:
            self.post_app_file()
        except AssertionError as e:
            print(f'{self.app_name} will not be updated : {e}')

    def upload_new_app(self):
        data = {
            "name": self.app_name,
            "type": self.device,
            "description": self.desc,
            "icon": self.icon
        }
        try:
            rq = requests.post(url=f"{self.target}api/apps/", headers=self.headers, data=json.dumps(data))
            assert rq.json()
            self.check_result(rq, name=stack()[0][3])
            self.app_uuid = rq.json()['id']
            print(f'New app created. id: {self.app_uuid}')
        except Exception as e:
            print(f"could not create new app: {e}")

    def run(self):
        try:
            self.assertions_on_inputs()
            if self.mode == 'upload':
                self.assertions_on_upload()
                self.upload_app()
            elif self.mode == 'upload_new':
                self.assertions_on_upload()
                self.assertion_on_type()
                self.upload_new_app()
                self.upload_app()
            elif self.mode == 'launch':
                self.assertions_on_campaign_launch()
                self.launch_campaign()
            elif self.mode == 'harvest':
                self.assertions_on_campaign_uuid()
                assert self.runid, "please provide run_id"
                self.check_campaign_run_evolution(self.runid)
        except AssertionError as e:
            print(f"error in main run : {e}")
            exit(-1)


def main():
    parser = argparse.ArgumentParser(description=USAGE, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-k', '--api_key', help="esChecker API_KEY", type=str)
    parser.add_argument('-a', '--app_uuid', help="Application UUID", action="store", type=str)
    parser.add_argument('-c', '--campaign_uuid', help="Campaign UUID", action="store", type=str)
    parser.add_argument('-f', '--app_file_path', help="file path", action="store", type=str)
    parser.add_argument('-t', '--timeout', help="maximum delay (s) until <app_name>-campain_uuid.json is collected",
                        action="store", type=int, default=200)
    parser.add_argument('-m', '--mode', help=f"modes to use API: supported modes are : {MODES}", action="store",
                        type=str)
    parser.add_argument('-d', '--device', help=f"supported devices are : {DEVICES_KIND}", action="store", type=str,
                        default='ANDROID')
    parser.add_argument('-n', '--name', help="The name of the app you want to create", action="store", type=str,
                        default="")
    parser.add_argument('-r', '--runid', help="The campaign run id you wan collect results from.", action="store",
                        type=str, default="")
    parser.add_argument('-p', '--description', help="Text describing your app", action="store",
                        default="App created using CI job",
                        type=str)
    parser.add_argument('-i', '--icon', help="The base64 string representing the icon", action="store", default="",
                        type=str)

    args = parser.parse_args()

    checker = CheckerCI(
        app_uuid=args.app_uuid,
        campaign_uuid=args.campaign_uuid,
        api_key=args.api_key,
        timeout=args.timeout,
        file_path=args.app_file_path,
        mode=args.mode,
        device=args.device,
        app_name=args.name,
        runid=args.runid,
        desc=args.desc,
        icon=args.icon
    )
    checker.run()


if __name__ == '__main__':
    main()
